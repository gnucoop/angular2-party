import {Component} from 'angular2/core';
import {Control, FormBuilder, ControlGroup, Validators, AbstractControl} from "angular2/common";
import {FORM_DIRECTIVES} from 'angular2/common';

@Component({
    selector: 'control-demo',
    directives: [FORM_DIRECTIVES],
    template: `
        <div class="ui top attached inverted header">Control Demo</div>
        <div class="ui attached segment">
            <div class="ui two columns grid">
                <div class="column">
                    <form novalidate class="ui form">
                        <div class="field">
                            <label>Foo</label>
                            <input type="text" [ngFormControl]="foo" />
                        </div>
                        <div (click)="foo.updateValue(foo.value)" class="ui button" tabindex="0">Submit</div>
                    </form>
                </div>
                <div class="column">
                    <div class="ui divided list">
                        <div class="item">
                            <div class="content">
                                <div class="header">foo.value</div>
                                <div class="description">{{ foo.value || '&nbsp;' }}</div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="content">
                                <div class="header">foo.status</div>
                                <div class="description">{{ foo.status }}</div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="content">
                                <div class="header">foo.valid</div>
                                <div class="description">{{ foo.valid ? 'true' : 'false' }}</div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="content">
                                <div class="header">foo.errors</div>
                                <div *ngIf="!foo.errors" class="description">&nbsp;</div>
                                <div *ngIf="foo.errors" class="description">
                                    <span *ngIf="foo.errors.hasOwnProperty('too_long')">Troppo lungo!</span>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="content">
                                <div class="header">foo.pristine</div>
                                <div class="description">{{ foo.pristine ? 'true' : 'false' }}</div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="content">
                                <div class="header">foo.dirty</div>
                                <div class="description">{{ foo.dirty ? 'true' : 'false' }}</div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="content">
                                <div class="header">foo.touched</div>
                                <div class="description">{{ foo.touched ? 'true' : 'false' }}</div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="content">
                                <div class="header">foo.untouched</div>
                                <div class="description">{{ foo.untouched ? 'true' : 'false' }}</div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="content">
                                <div class="header">foo.pending</div>
                                <div class="description">{{ foo.pending ? 'true' : 'false' }}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    `
})
class ControlDemoComponent {
    foo: Control = new Control('foo', function(x): {[key: string]: any} {
        return x.value.length < 10 ? {} : {'too_long': true};
    });
}

@Component({
    selector: 'form-demo',
    directives: [FORM_DIRECTIVES],
    template: `
        <div class="ui top attached inverted header">Form Demo</div>
        <div class="ui attached segment">
            <div class="ui two columns grid">
                <div class="column">
                    <form #f="ngForm" (ngSubmit)="onSubmit(f.value)" class="ui form">
                        <div class="field">
                            <label for="foo">Foo</label>
                            <input id="foo" type="text" ngControl="foo" />
                        </div>
                        <div class="field">
                            <label for="bar">Bar</label>
                            <input id="bar" type="text" ngControl="bar" />
                        </div>
                        <div class="field">
                            <label for="baz">Baz</label>
                            <input id="baz" type="text" ngControl="baz" />
                        </div>
                        <button class="ui button" type="submit">Submit</button>
                    </form>
                </div>
                <div class="column">
                    <label>Form value</label>
                    <pre>{{ formValue }}</pre>
                </div>
            </div>
        </div>
    `
})
class FormDemoComponent {
    formValue: string = '';

    onSubmit(value: any) {
        this.formValue = JSON.stringify(value);
    }
}

@Component({
    selector: 'smart-form-demo',
    directives: [FORM_DIRECTIVES],
    template: `
        <div class="ui top attached inverted header">Smart Form Demo</div>
        <div class="ui attached segment">
            <div class="ui two columns grid">
                <div class="column">
                    <form [ngFormModel]="smartForm" (ngSubmit)="onSubmit(smartForm.value)" class="ui form">
                        <div class="field">
                            <label for="name">Name</label>
                            <input id="name" type="text" [ngFormControl]="smartForm.controls['name']" />
                        </div>
                        <div class="field">
                            <label for="nickname">Nickname</label>
                            <input id="nickname" type="text" [ngFormControl]="nickname" />
                            <div *ngIf="nickname.hasError('required')">devi inserirmi!</div>
                            <div *ngIf="nickname.hasError('only_cool')">solo nomi fighi!</div>
                        </div>
                        <div class="field">
                             <label for="last_name">Last name</label>
                             <input id="last_name" type="text" [(ngModel)]="last_name" />
                        </div>
                        <button class="ui button" type="submit">Submit</button>
                    </form>
                </div>
                <div class="column">
                    <label>Form value</label>
                    <pre>{{ formValue }}</pre>
                    <pre>{{ smartForm.dirty }}</pre>
                    <pre>{{num_nickname}}</pre>
                </div>
            </div>
        </div>
    `
})
class SmartFormDemoComponent {
    smartForm: ControlGroup;
    nickname: AbstractControl;
    formValue: string;
    num_nickname: number;
    last_name: string;

    constructor(fb: FormBuilder) {
        this.smartForm = fb.group({
            'name': ['foo', Validators.required],
            'nickname': ['bar', Validators.compose([
                Validators.required,
                function(x): {[key: string]: any} {
                    if(x.value.length >= 3)
                        return {'only_cool': true};
                }
            ])]
        });
        this.nickname = this.smartForm.controls['nickname'];

        var self = this;
        this.nickname.valueChanges.subscribe((x) => {
            self.num_nickname = x.length;
        });

        this.smartForm.valueChanges.subscribe((x) => {
            x.name = 'daniel';
        });

        this.last_name = 'gnucoop';

        setTimeout(function() {
            self.smartForm.controls['name'].value = 'miao';
        }, 3000);
    }

    onSubmit(value: any) {
        this.formValue = JSON.stringify(value);
    }
}

@Component({
    selector: 'app',
    directives: [ControlDemoComponent, FormDemoComponent, SmartFormDemoComponent],
    template: `
        <div class="ui container">
            <h1>Angular 2 Party - Form in Angular 2</h1>
            <control-demo></control-demo>
            <br>
            <form-demo></form-demo>
            <br>
            <smart-form-demo></smart-form-demo>
        </div>
    `
})
export class AppComponent {
}
