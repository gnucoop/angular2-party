import {Component, EventEmitter} from 'angular2/core';

class AjfCondition {

}

class AjfFormula {

}

class AjfField {
    id: number;
    name: string;
    label: string;
    description: string;
    visibility: AjfCondition;
    conditionalBranches: AjfCondition[];
    editable: boolean;
    formula: AjfFormula;
    hasChoices: boolean;

    constructor(obj?: any) {
        this.id                  = obj && obj.id                  || null;
        this.name                = obj && obj.name                || null;
        this.label               = obj && obj.label               || null;
        this.description         = obj && obj.description         || null;
        this.visibility          = obj && obj.visibility          || false;
        this.conditionalBranches = obj && obj.conditionalBranches || null;
        this.editable            = obj && obj.editable            || false;
        this.formula             = obj && obj.formula             || null;
        this.hasChoices          = obj && obj.hasChoices          || false;
    }
}

class AjfForm {
    fields: AjfField[];

    constructor(obj?: any) {
        this.fields = obj && obj.fields || [];
    }
}

@Component({
    'selector': 'ajf-field',
    'inputs': ['field'],
    'outputs': ['fieldSelected'],
    'template': `
        <div class="content">
            <a (click)="onFieldClick($event)" class="header">{{ field.name }}</a>
            {{ field.label }} <em>{{ field.description }}</em>
        </div>
	`
})
class AjfFieldComponent {
    field: AjfField;
    fieldSelected: EventEmitter<AjfField> = new EventEmitter();

    constructor() {
        this.fieldSelected.subscribe((e: AjfField) => { console.log(e.name); })
    }

    onFieldClick(event) {
        this.fieldSelected.emit(this.field);
    }
}

@Component({
    'selector': 'ajf-form',
    'inputs': ['form'],
    'directives': [AjfFieldComponent],
    'template': `
		<div class="ui list">
			<ajf-field class="item" (fieldSelected)="onFieldSelected($event)" *ngFor="#myField of form.fields" [field]="myField"></ajf-field>
		</div>
`
})
class AjfFormComponent {
    form: AjfForm;

    onFieldSelected(field: AjfField) {
        this.form.fields = this.form.fields.filter((x) => x != field);
    }
}

@Component({
    selector: 'ajf-builder-designer',
    directives: [AjfFormComponent],
    template: `
		<div class="ui segment">
			<h3 class="ui header">Form designer</h3>
			<div class="content">
				<ajf-form [form]="myForm"></ajf-form>
			</div>
			<!-- condition editor -->
			<!-- formula editor -->
		</div>
	`
})
class AfjBuilderDesigner {
    myForm: AjfForm = new AjfForm({
        fields: [
            new AjfField({'name': 'first_name', 'label': 'First name', 'description': 'Your first name'}),
            new AjfField({'name': 'last_name', 'label': 'Last name', 'description': 'Your last name'}),
            new AjfField({'name': 'age', 'label': 'Age', 'description': 'Your age'})
        ]
    });
}

@Component({
    selector: 'app',
    directives: [AfjBuilderDesigner],
    template: `
        <h1>Angular 2 Party App</h1>
        <ajf-builder-designer></ajf-builder-designer>
    `
})
export class AppComponent {
    newField: AjfField = new AjfField({'name': 'prova', 'label': 'lbl', 'description': 'a super new field'});
}
