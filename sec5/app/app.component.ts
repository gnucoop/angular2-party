import {Component, EventEmitter, ElementRef, ViewChild, Query} from "angular2/core";
import {Observable} from "rxjs/Rx";
import {HTTP_PROVIDERS, Http} from "angular2/http";
import {Subject} from "rxjs/Subject";

declare var jQuery: any;

@Component({
    selector: 'reactive-component-1',
    template: `
        <div class="ui top attached inverted segment">
            <button (click)="startFlow()" class="ui right floated button">Flow!</button>
            Reactive Component 1
        </div>
        <div class="ui attached segment">
            <div style="height: 350px; overflow-y: auto;" class="content">
                <pre *ngFor="#rs of requestStrings">{{ rs }}</pre>
            </div>
        </div>
    `
})
class ReactiveComponent1 {
    requestStrings: string[] = [];

    constructor() {

    }

    startFlow() {
        var self = this;

        var requestStream: Observable<string> = Observable.fromArray<string>(['https://api.github.com/users']);

        requestStream.subscribe(function(requestUrl) {
            // execute the request
            self.requestStrings.push(`<div>url: ${requestUrl}</div>`);
        }, function() {
            self.requestStrings.push(`<div>errore</div>`);
        }, function() {
            self.requestStrings.push(`<div>finito</div>`);
        });
    }
}

@Component({
    selector: 'reactive-component-2',
    template: `
        <div class="ui top attached inverted segment">
            <button (click)="startFlow()" class="ui right floated button">Flow!</button>
            Reactive Component 2
        </div>
        <div class="ui attached segment">
            <div class="ui grid two column">
                <div class="column">
                    <div class="ui header">Request</div>
                    <div style="height: 350px; overflow-y: auto;" class="content">
                        <pre *ngFor="#rs of requestStrings">{{ rs }}</pre>
                    </div>
                </div>
                <div class="column">
                    <div class="ui header">Response</div>
                    <div style="height: 350px; overflow-y: auto;" class="content">
                        <pre *ngFor="#rs of responseStrings">{{ rs }}</pre>
                    </div>
                </div>
            </div>
        </div>
    `
})
class ReactiveComponent2 {
    requestStrings: string[] = [];
    responseStrings: string[] = [];

    constructor() {

    }

    startFlow() {
        var self = this;

        var requestStream: Observable<string> = Observable.fromArray<string>(['https://api.github.com/users']);

        requestStream.subscribe(function(requestUrl) {
            // execute the request
            self.requestStrings.push(`url: ${requestUrl}`);
            var responseStream = Observable.create(function (observer) {
                jQuery.getJSON(requestUrl)
                    .done(function(response) { observer.next(response); })
                    .fail(function(jqXHR, status, error) { observer.error(error); })
                    .always(function() { observer.complete(); });
            });

            responseStream.subscribe(function(response) {
                // do something with the response
                self.responseStrings.push(`${JSON.stringify(response)}`);
            }, function() {
                self.responseStrings.push(`errore`);
            }, function() {
                self.responseStrings.push(`finito`);
            });
        }, function() {
            self.requestStrings.push(`errore`);
        }, function() {
            self.requestStrings.push(`finito`);
        });
    }
}

@Component({
    selector: 'reactive-component-3',
    template: `
        <div class="ui top attached inverted segment">
            <button (click)="startFlow()" class="ui right floated button">Flow!</button>
            Reactive Component 3
        </div>
        <div class="ui attached segment">
            <div class="ui grid three column">
                <div class="column">
                    <div class="ui header">Request</div>
                    <div style="height: 350px; overflow-y: auto;" class="content">
                        <pre *ngFor="#rs of requestStrings">{{ rs }}</pre>
                    </div>
                </div>
                <div class="column">
                    <div class="ui header">Meta-response</div>
                    <div style="height: 350px; overflow-y: auto;" class="content">
                        <pre *ngFor="#rs of metaStrings">{{ rs }}</pre>
                    </div>
                </div>
                <div class="column">
                    <div class="ui header">Response</div>
                    <div style="height: 350px; overflow-y: auto;" class="content">
                        <pre *ngFor="#rs of responseStrings">{{ rs }}</pre>
                    </div>
                </div>
            </div>
        </div>
    `
})
class ReactiveComponent3 {
    requestStrings: string[] = [];
    responseStrings: string[] = [];
    metaStrings: string[] = [];

    constructor() {

    }

    startFlow() {
        var self = this;

        var requestStream: Observable<string> = Observable.fromArray<string>(['https://api.github.com/users']);

        var responseMetastream = requestStream.map(function(requestUrl) {
            return Observable.fromPromise(jQuery.getJSON(requestUrl));
        });

        responseMetastream.subscribe(function(responseStream) {
            self.metaStrings.push(`${JSON.stringify(responseStream)}`);

            responseStream.subscribe(function(response) {
                // do something with the response
                self.responseStrings.push(`${JSON.stringify(response)}`);
            }, function() {
                self.responseStrings.push(`errore`);
            }, function() {
                self.responseStrings.push(`finito`);
            });
        }, function() {
            self.metaStrings.push(`errore`);
        }, function() {
            self.metaStrings.push(`finito`);
        });


        requestStream.subscribe(function(requestUrl) {
            self.requestStrings.push(`${requestUrl}`);
        }, function() {
            self.requestStrings.push(`errore`);
        }, function() {
            self.requestStrings.push(`finito`);
        });
    }
}

@Component({
    selector: 'reactive-component-4',
    template: `
        <div class="ui top attached inverted segment">
            <button (click)="startFlow()" class="ui right floated button">Flow!</button>
            Reactive Component 4
        </div>
        <div class="ui attached segment">
            <div class="ui grid two column">
                <div class="column">
                    <div class="ui header">Request</div>
                    <div style="height: 350px; overflow-y: auto;" class="content">
                        <pre *ngFor="#rs of requestStrings">{{ rs }}</pre>
                    </div>
                </div>
                <div class="column">
                    <div class="ui header">Response</div>
                    <div style="height: 350px; overflow-y: auto;" class="content">
                        <pre *ngFor="#rs of responseStrings">{{ rs }}</pre>
                    </div>
                </div>
            </div>
        </div>
    `
})
class ReactiveComponent4 {
    requestStrings: string[] = [];
    responseStrings: string[] = [];

    constructor() {

    }

    startFlow() {
        var self = this;

        var requestStream: Observable<string> = Observable.fromArray<string>(['https://api.github.com/users']);

        var responseStream = requestStream.flatMap(function(requestUrl) {
            return Observable.fromPromise(jQuery.getJSON(requestUrl));
        });

        requestStream.subscribe(function(requestUrl) {
            self.requestStrings.push(`${requestUrl}`);
        }, function() {
            self.requestStrings.push(`errore`);
        }, function() {
            self.requestStrings.push(`finito`);
        });

        responseStream.subscribe(function(response) {
            // do something with the response
            self.responseStrings.push(`${JSON.stringify(response)}`);
        }, function() {
            self.responseStrings.push(`errore`);
        }, function() {
            self.responseStrings.push(`finito`);
        });
    }
}

@Component({
    selector: 'reactive-component-5',
    viewProviders: [HTTP_PROVIDERS],
    template: `
        <div class="ui top attached inverted segment">
            <button (click)="startFlow()" class="ui right floated button">Flow!</button>
            Reactive Component 5
        </div>
        <div class="ui attached segment">
            <div class="ui grid two column">
                <div class="column">
                    <div class="ui header">Request</div>
                    <div style="height: 350px; overflow-y: auto;" class="content">
                        <pre *ngFor="#rs of requestStrings">{{ rs }}</pre>
                    </div>
                </div>
                <div class="column">
                    <div class="ui header">Response</div>
                    <div style="height: 350px; overflow-y: auto;" class="content">
                        <pre *ngFor="#rs of responseStrings">{{ rs }}</pre>
                    </div>
                </div>
            </div>
        </div>
    `
})
class ReactiveComponent5 {
    requestStrings: string[] = [];
    responseStrings: string[] = [];

    constructor(private http: Http) {

    }

    startFlow() {
        var self = this;

        var requestStream: Observable<string> = Observable.fromArray<string>(['https://api.github.com/users']);

        var responseStream = requestStream.flatMap(function(requestUrl) {
            return self.http.get(requestUrl);
        });

        requestStream.subscribe(function(requestUrl) {
            self.requestStrings.push(`${requestUrl}`);
        }, function() {
            self.requestStrings.push(`errore`);
        }, function() {
            self.requestStrings.push(`finito`);
        });

        responseStream.subscribe(function(response) {
            // do something with the response
            self.responseStrings.push(`${JSON.stringify(response)}`);
        }, function() {
            self.responseStrings.push(`errore`);
        }, function() {
            self.responseStrings.push(`finito`);
        });
    }
}

@Component({
    selector: 'reactive-component-6',
    viewProviders: [HTTP_PROVIDERS],
    template: `
        <div class="ui top attached inverted segment">
            <button (click)="startFlow()" class="ui right floated button">Flow!</button>
            Reactive Component 6
        </div>
        <div class="ui attached segment">
            <div class="ui grid two column">
                <div class="column">
                    <div class="ui header">Cold Observable</div>
                    <div style="height: 350px; overflow-y: auto;" class="content">
                        <pre *ngFor="#cs of coldStrings">{{ cs }}</pre>
                    </div>
                </div>
                <div class="column">
                    <div class="ui header">Hot Observable</div>
                    <div style="height: 350px; overflow-y: auto;" class="content">
                        <pre *ngFor="#hs of hotStrings">{{ hs }}</pre>
                    </div>
                </div>
            </div>
        </div>
    `
})
class ReactiveComponent6 {
    coldStrings: string[] = [];
    hotStrings: string[] = [];

    prova: Subject;

    constructor(private http: Http) {
    }

    startFlow() {
        var self = this;

        var cold = Observable.interval(1000);
        var hot = cold.publish();

        hot.connect();

        setTimeout(function() {
            cold.subscribe(function(x) {
                self.coldStrings.push(`${x}`);
            });

            hot.subscribe(function(x) {
                self.hotStrings.push(`${x}`);
            });
        }, 2500);
    }
}

@Component({
    selector: 'app',
    directives: [
        ReactiveComponent1, ReactiveComponent2, ReactiveComponent3, ReactiveComponent4,
        ReactiveComponent5, ReactiveComponent6
    ],
    template: `
        <div class="ui container">
            <reactive-component-1></reactive-component-1>
            <reactive-component-2></reactive-component-2>
            <reactive-component-3></reactive-component-3>
            <reactive-component-4></reactive-component-4>
            <reactive-component-5></reactive-component-5>
            <reactive-component-6></reactive-component-6>
        </div>
    `
})
export class AppComponent {
}