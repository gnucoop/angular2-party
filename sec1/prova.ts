interface IFoo {
	bar(): number;
}

class Foo implements IFoo {
	private _x: boolean;

	constructor() {
		this._x = false;
	}
	bar(): number {
		return 1;
	}
}
